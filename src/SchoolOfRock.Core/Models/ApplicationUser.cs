﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolOfRock.Core.Models
{
    public class ApplicationUser : IdentityUser
    {
        public ICollection<ScheduleUser> Tranings { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
    }
}
