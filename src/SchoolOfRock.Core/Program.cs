﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore;
using SchoolOfRock.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolOfRock.Core
{
    public class Program
    {

        public class MigrationsContextFactory : IDbContextFactory<ApplicationDBContext>
        {
            public ApplicationDBContext Create(DbContextFactoryOptions options)
            {
                var builder = new DbContextOptionsBuilder<ApplicationDBContext>();
                builder.UseSqlServer("Server=localhost;Database=sor;Integrated Security=yes;Trusted_Connection=True;MultipleActiveResultSets=true;", b => b.MigrationsAssembly("SchoolOfRock.Web"));
                return new ApplicationDBContext(builder.Options);
            }
        }
    }
}
